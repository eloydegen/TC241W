# Samsung TC241W 
[Specification sheet](https://archive.org/details/manualzilla-id-7113418/page/n1/mode/2up)

In case your TC241W is locked down and has Windows 7 installed and would like to to install a different operating system, you can choose between disassembling the machine and remove the UEFI password to allow USB booting, or setup a PXE server to boot from the network.

## Password reset 
* Remove the 4 screws to remove the desk VESA mount
* Use some thin utility such as a Stanley knife to disconnect the clips in between the back and front plastic parts.
* Disconnect the cables
* Remove the hex jackscrews from the sides, such as the VGA and serial port
* Turn the mainboard around
* Remove DRAM SO-DIMM 
* Remove the CR2032 battery, which will reset the UEFI password.
* Re-assemble the machine 
* Press F2 to enter the UEFI setup

## PXE boot 
To enter the boot menu, press F5. I recommend [netboot.xyz](https://netboot.xyz) for netbooting.

## EEPROM notes
![EEPROM](/EEPROM.JPG)
* Datasheet: https://datasheetspdf.com/pdf-file/1469559/Winbond/25X40BVSIG/1

```
[eloy@t480 minipro]$ sudo ./minipro --device "W25X40BV@SOIC8" --read thinclient.rom
Found TL866II+ 04.2.128 (0x280)
Overcurrent protection!
```

This is happening when using a clip, probably due to current that is leaking to other parts of the mainboard. The uefi.rom in this repo has been extracted with internal flashing, but is incomplete due to protected regions.
